from adapt.intent import IntentBuilder
from mycroft.util.format import (nice_date, nice_duration, nice_time,
                                 date_time_format)
from mycroft.util.time import now_utc, to_local, now_local
from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import LOG
import random

class WaitForLight(MycroftSkill):

    def __init__(self):
        super(WaitForLight, self).__init__(name="WaitForLight")

    #Respond to the user asking how long he has to wait before the lught turns green with a random amount seconds
    @intent_handler(IntentBuilder('').require('Moving_options').optionally('Query').optionally('Traffic_colour'))
    def handle_travel_advice(self, message):
        utt = message.data.get('utterance', "").lower()
        seconds = random.randrange(1, 10)
        #self.speak_dialog("you have to wait {} seconds before you can cross".format(seconds))
        self.speak_dialog("waiting.time", data={"time" : seconds})

def create_skill():
    return WaitForLight()
